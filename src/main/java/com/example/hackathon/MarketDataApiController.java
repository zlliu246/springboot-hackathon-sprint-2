package com.example.hackathon;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.ParameterizedType;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MarketDataApiController {

    @Autowired
    private UserService service;

    public boolean checkUser(User targetUser) {

        boolean valid = false;
        for (User user: service.getAllUsers()) {
            if (user.getUsername().equals(targetUser.getUsername()) && user.getPassword().equals(targetUser.getPassword())){
                valid = true;
            }
        }
        return valid;
    }

    @GetMapping("/stock")
    public ResponseEntity getStock(@RequestParam String symbol, @RequestBody User user) {

        if (!checkUser(user)) {
            return new ResponseEntity("User authentication failed", HttpStatus.FORBIDDEN);
        }

        try {

            String url = String.format("https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v3/get-historical-data?symbol=%s&region=US", symbol);

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .header("x-rapidapi-key", "8aaaefecbemsh2ad8bd7073ec219p11733fjsn987904475540")
                    .header("x-rapidapi-host", "apidojo-yahoo-finance-v1.p.rapidapi.com")
                    .method("GET", HttpRequest.BodyPublishers.noBody())
                    .build();

            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

            return new ResponseEntity(response.body(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity("Error", HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/cryptocurrency")
    public ResponseEntity getCrypto(@RequestParam String symbol, @RequestBody User user) {

        if (!checkUser(user)) {
            return new ResponseEntity(new UserAuthenticationException(), HttpStatus.FORBIDDEN);
        };

        String coinListUrl = "https://api.coingecko.com/api/v3/coins/list";

        RestTemplate template = new RestTemplate();

        ParameterizedTypeReference<List<Coin>> responseType = new ParameterizedTypeReference<List<Coin>>(){};
        ResponseEntity<List<Coin>> response = template.exchange(coinListUrl, HttpMethod.GET, null, responseType);

        Map<String, String> map = new HashMap<>();

        for (Coin coin: response.getBody()) {
            map.put(coin.getSymbol(), coin.getId());
        }

        String priceUrl = String.format("https://api.coingecko.com/api/v3/coins/%s/market_chart?vs_currency=sgd&days=1", map.get(symbol.toLowerCase()));

        CryptoMarketPriceResponse cryptoMarketPriceResponse = template.getForObject(priceUrl, CryptoMarketPriceResponse.class);
        return new ResponseEntity(cryptoMarketPriceResponse, HttpStatus.OK);
    }

}

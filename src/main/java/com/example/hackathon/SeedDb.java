package com.example.hackathon;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
public class SeedDb {
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void init() {
        jdbcTemplate.update("insert into USER (username, password) values (?, ?)", new Object[]{"username1", "password1"});
        jdbcTemplate.update("insert into USER (username, password) values (?, ?)", new Object[]{"username2", "password2"});
        jdbcTemplate.update("insert into USER (username, password) values (?, ?)", new Object[]{"username3", "password3"});
        jdbcTemplate.update("insert into USER (username, password) values (?, ?)", new Object[]{"username4", "password4"});
    }
}

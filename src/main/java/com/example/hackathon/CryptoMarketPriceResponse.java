package com.example.hackathon;

import java.util.List;

public class CryptoMarketPriceResponse {

    private List<List<Double>> prices;
    private List<List<Double>> market_caps;
    private List<List<Double>> total_volumes;

    public List<List<Double>> getPrices() {return prices;}
    public List<List<Double>> getMarketCaps() {return market_caps;}
    public List<List<Double>> getTotalVolumes() {return total_volumes;}

}

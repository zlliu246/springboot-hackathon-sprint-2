package com.example.hackathon;

public class Coin {

    private String id;
    private String symbol;
    private String name;

    public String getId() {return id;}
    public String getSymbol() {return symbol;}
    public String getName() {return name;}

}
